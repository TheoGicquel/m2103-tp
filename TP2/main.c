#include <stdio.h>
#include <stdlib.h>

void print_tableau(char tableau[])
{
    int index;
    for (index=0;tableau[index]!='\0';index++)
    {
        printf("%c", tableau[index]);
    }
}

void init_string(char tableau[])
{
    printf("Saisissez une chaine de caracteres : " );
    gets(tableau);
}

void init_string_char_by_char(char tableau[])
{
    char temp_char;
    char choix;
    int compteur=0;
    do
    {
        printf("Saisissez le %d e caractere :", (compteur+1));
        scanf("\n");
        temp_char=getchar();
        tableau[compteur]=temp_char;
        compteur++;
        printf("Continuer (O/N) ?");
        scanf("\n");
        choix=getchar();
    }while(choix!='N');
    tableau[compteur]='\0';
}

void caps_lock(char tableau[])
{
    int index;
    for(index=0;tableau[index]!='\0';index++)
    {
        tableau[index]=toupper(tableau[index]);
    }
}

int char_count(char tableau[])
{
    int count=0,index;
    for (index=0;tableau[index]!='\0';index++)
    {
        count++;
    }
    return count;
}

void copy_string(char tableau_source[], char tableau_destination[]) //par pointeur
{
    char * pointeur_s, * pointeur_d;
    pointeur_d=tableau_destination;
    for (pointeur_s=tableau_source; *pointeur_s!='\0'; pointeur_s++)
        {
            *pointeur_d=*pointeur_s;
            pointeur_d++;
        }
}

void concatene_moi_cette_merde(char tableau_debut[],char tableau_fin[],char tableau_sortie[])
{
    int index_entree, index_sortie;
    index_sortie=0 ;
    for (index_entree=0; tableau_debut[index_entree]!='\0'; index_entree++)
    {
        tableau_sortie[index_sortie]=tableau_debut[index_entree];
        index_sortie++;
    }
    for (index_entree=0; tableau_fin[index_entree]!='\0'; index_entree++)
    {
        tableau_sortie[index_sortie]=tableau_fin[index_entree];
        index_sortie++;
    }
    tableau_sortie[index_sortie]='\0';
}

void reverse_string(char tableau[])
{
    char copy_table[100];
    int index, limit_index;
    limit_index=0;
    copy_string(tableau,copy_table);
    for (index=0;copy_table[index]!='\0';index++)
    {
        limit_index++;
    }
    for (index=0;copy_table[index]!='\0';index++)
    {
        tableau[limit_index-index-1]=copy_table[index];
    }
}

int main()
{
    char char_table[100],char_table2[100],char_table3[100];
    return 0;
}
