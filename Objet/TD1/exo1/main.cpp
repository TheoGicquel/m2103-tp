//Made by Xzing, shitty C++ code
#include <iostream>

using namespace std;

class point
{
    private :
        float x, y;
    public :
        point();
        point(float);
        point(float, float);
        ~point();
        void affiche();
        void deplace(int, int);
        void homothetie (int);
};

point::point()
{
    x=0;
    y=0;
}

point::point(float pos)
{
    x=pos;
    y=pos;
}

point::point(float x_pos, float y_pos)
{
    x=x_pos;
    y=y_pos;
}

point::~point()
{
    //cout << "destructeur" << endl;
}

void point::affiche()
{
    cout << "X : " << this->x << " Y : " << this->y <<endl;
}

void point::deplace(int dx, int dy)
{
    x+=dx;
    y+=dy;
}

void point::homothetie(int facteur)
{
    x*=facteur;
    y*=facteur;
}

int main()
{
    point test_point(2,3);
    test_point.affiche();
    test_point.deplace(3,-1);
    test_point.affiche();
    test_point.homothetie(3);
    test_point.affiche();
    return 0;
}
