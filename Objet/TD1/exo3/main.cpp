#include <iostream>

#include <vector> //pour la gestion dynamique

using namespace std;

class vecteur2
{
    private:
        int nb_valeurs;
    public:  
        vecteur2(int);
        vecteur2(int, int*);
        ~vecteur2();
        void init();
        void affiche();
        void homothetie(int);
        int produit_scalaire(vecteur2);   
        static void affiche_nb_instances();
        static int nb_instances;
        std::vector<int> valeurs;
};

int vecteur2::nb_instances = 0;

vecteur2::vecteur2(int nb_vals_nulles)
{
    nb_valeurs = nb_vals_nulles;
    for (int index=0; index<=nb_valeurs; index++)
    {
        valeurs[index]=0;
    }
    nb_instances++;
}

vecteur2::vecteur2(int nb_vals, int *tableau)
{
    nb_valeurs = nb_vals;
    for (int index=0; index<=nb_valeurs; index++)
    {
        valeurs[index]=tableau[index];
    }
    nb_instances++;
}

vecteur2::~vecteur2(){}

void vecteur2::affiche()
{
    for (int index=0; index<=nb_valeurs; index++)
    {
        cout << "valeur n" << index << " : "<< valeurs[index] << endl;
    }
}

void vecteur2::init()
{
    cout<< "Combien de vals?" <<endl;
    cin>> nb_valeurs;
    for (int index=0; index<=nb_valeurs; index++)
    {
        cout<< "Saisisez la valeur n" << index << ":" <<endl;
        cin>> valeurs[index];
    }
    cout<< "Saisie terminee !" <<endl;
    nb_instances++;
}

void vecteur2::homothetie(int facteur)
{
    for (int index=0; index<=nb_valeurs; index++)
    {
        valeurs[index]*=facteur;
    }
}

int vecteur2::produit_scalaire(vecteur2 scalaire)
{
    int total=0;
    for (int index=0; index<=nb_valeurs; index++)
    {
        total=total+(valeurs[index]*scalaire.valeurs[index]);
    }
    return total;
}

void vecteur2::affiche_nb_instances()
{
    cout<<"Il y a"<<vecteur2::nb_instances<<"instances";
}

int main()
{
    int test_tabl [5] = {1,2,3,4,5};
    vecteur2 test_vecteur(5, test_tabl);
    test_vecteur.affiche();
    test_vecteur.init();
    test_vecteur.affiche();
    test_vecteur.homothetie(10);
    test_vecteur.affiche();
    cout<<test_vecteur.produit_scalaire(test_vecteur);
    vecteur2::affiche_nb_instances;
    return 0;
}
