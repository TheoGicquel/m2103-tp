#include <iostream>

using namespace std;

class vecteur
{
    private:
        int nb_valeurs;
        int valeurs[50];
    public:
        vecteur(int);
        vecteur(int, int*);
        ~vecteur();
        void init();
        void affiche();
        void homothetie(int);
        int produit_scalaire(vecteur);
};

vecteur::vecteur(int nb_vals_nulles)
{
    nb_valeurs = nb_vals_nulles;
    for (int index=0; index<=nb_valeurs; index++)
    {
        valeurs[index]=0;
    }
}

vecteur::vecteur(int nb_vals, int *tableau)
{
    nb_valeurs = nb_vals;
    for (int index=0; index<=nb_valeurs; index++)
    {
        valeurs[index]=tableau[index];
    }
}

vecteur::~vecteur(){}

void vecteur::affiche()
{
    for (int index=0; index<=nb_valeurs; index++)
    {
        cout << "valeur n" << index << " : "<< valeurs[index] << endl;
    }
}

void vecteur::init()
{
    cout<< "Combien de vals?" <<endl;
    cin>> nb_valeurs;
    for (int index=0; index<=nb_valeurs; index++)
    {
        cout<< "Saisisez la valeur n" << index << ":" <<endl;
        cin>> valeurs[index];
    }
    cout<< "Saisie terminee !" <<endl;
}

void vecteur::homothetie(int facteur)
{
    for (int index=0; index<=nb_valeurs; index++)
    {
        valeurs[index]*=facteur;
    }
}

int vecteur::produit_scalaire(vecteur scalaire)
{
    int total=0;
    for (int index=0; index<=nb_valeurs; index++)
    {
        total=total+(valeurs[index]*scalaire.valeurs[index]);
    }
    return total;
}

int main()
{
    int test_tabl [5] = {1,2,3,4,5};
    vecteur test_vecteur(5, test_tabl);
    test_vecteur.affiche();
    test_vecteur.init();
    test_vecteur.affiche();
    test_vecteur.homothetie(10);
    test_vecteur.affiche();
    cout<<test_vecteur.produit_scalaire(test_vecteur);
    return 0;
}
