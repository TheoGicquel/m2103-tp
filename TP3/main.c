#include <stdio.h>
#include <stdlib.h>

int tabl_size(char *tableau[]);
void saisie_string(char *tableau[]);
void print_string(char *tableau[]);
void del_all_char (char *tableau[], char todel_char);
void del_index(char *tableau[], int rang);
void double_all_char (char *tableau[], char todouble_char);
void add_char_to_index(char *tableau[], int rang, char toadd_char);
void double_every_char (char *tableau[]);

int tabl_size(char *tableau[])
{
    int index=0;
    while((*tableau)[index]!='\0')
    {
        index++;
    }
    return index;
}

void saisie_string(char *tableau[])
{
    char choix, char_saisi;
    char *temp_adresse;
    int index=0, index_copy;
    printf("Saisie d'une chaine de caracteres : \n " );
    do
    {
        if (index)
        {
            temp_adresse=*tableau;
        }
        (*tableau) = malloc((index+2)*sizeof(char));
        for(index_copy=0;index_copy<index;index_copy++)
        {
            (*tableau)[index_copy] = *temp_adresse;
            temp_adresse++;
        }
        printf("   Saisissez le caractere %i : ", (index+1) );
        scanf("\n");
        char_saisi=getchar();
        (*tableau)[index]=char_saisi;
        index++;
        (*tableau)[index]='\0';
        printf("Continuer (O/N) ? ");
        scanf("\n");
        choix=getchar();
    }while(choix!='N');
}

void print_string(char *tableau[])
{
    for (int index=0; (*tableau)[index]!='\0';index++)
    {
        printf("%c", (*tableau)[index]);
    }
    printf("\n");
}

void del_all_char (char *tableau[], char todel_char)
{
    int index=0;
    while(index<tabl_size(tableau))
    {
        if ((*tableau)[index]==todel_char)
        {
            del_index(tableau, index);
        }
        index++;
    }
}

void double_all_char (char *tableau[], char todouble_char)
{
    int index=0;
    while(index<tabl_size(tableau))
    {
        if ((*tableau)[index]==todouble_char)
        {
            add_char_to_index(tableau, index, todouble_char);
            index++;
        }
    index++;
    }
}

void del_index(char *tableau[], int rang)
{
    char *temp_adresse;
    int size = (tabl_size(tableau));
    int del_char_passed = 0;
    temp_adresse=*tableau;
    (*tableau) = malloc((size)*sizeof(char));

    for (int index=0; index<size; index++)
    {
        if (index!=rang)
        {
            (*tableau)[index-del_char_passed] = *temp_adresse;
        }
        else
        {
            del_char_passed = 1;
        }
        temp_adresse++;
    }
    (*tableau)[size-1]='\0';
}


void add_char_to_index(char *tableau[], int rang, char toadd_char)
{
    char *temp_adresse;
    int size = (tabl_size(tableau)+1);
    temp_adresse=*tableau;
    (*tableau) = malloc((size+1)*sizeof(char));

    for (int index=0; index<size+1; index++)
    {
        if (index==rang)
        {
            (*tableau)[rang] = toadd_char;
        }
        else
        {
            (*tableau)[index] = *temp_adresse;
            temp_adresse++;
        }
    }
    (*tableau)[size+1]='\0';

}

void double_every_char (char *tableau[])
{
    int init_size = tabl_size(tableau);
    for (int index = 0; index < init_size; index++)
    {
        add_char_to_index(tableau, index*2, (*tableau)[index*2]);
    }
}

int main()
{
    char *test_tabl = NULL;
    saisie_string(&test_tabl);
    print_string(&test_tabl);
    double_every_char(&test_tabl);
    print_string(&test_tabl);
    free(test_tabl);
    return 0;
}
